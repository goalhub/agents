:-dynamic square/2.

possiblemove(left).
possiblemove(right).
possiblemove(back).
possiblemove(forward).

% contents of squares that we can't go to.
obstruction(vac). % a bot is there
obstruction(obstacle).